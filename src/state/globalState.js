import { hookstate, useHookstate } from '@hookstate/core';

const globalState = hookstate({});

export default globalState