interface WorkSpace {
  id: string
  name: string,
  type: string,
  description?: string,
}

export default WorkSpace;