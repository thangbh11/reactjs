import WorkSpaceTables from "../../components/WorkSpaceTables";
import globalState from "../../state/globalState";
import SideBar from "../../components/SideBar";
import React from "react";

function Home () {
  const workSpaces = globalState.workSpaces.get()

  return (
    <>
      <div className="container content">
        <div className="row">
          <div className="col-3">
            <SideBar />
          </div>
          <div className="col-9">
            <h3 className="title">YOUR WORKSPACES</h3>
            {
              workSpaces && workSpaces.map((item, key) => (
                <WorkSpaceTables key={key} workSpace={item}/>
              ))
            }
          </div>
        </div>
      </div>
    </>
  )
}

export default Home;
