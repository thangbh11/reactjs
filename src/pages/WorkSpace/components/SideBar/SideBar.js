import './SideBar.scss'
import {Link} from "react-router-dom";

function SideBar (props) {
  return (
    <div className={'w-sidebar'}>
      <div className="__top">
        <div className={'icon-name text-center bg-0'}>
          <b>T</b>
        </div>
        <b className={'text-dark __name'}>Tabu</b>
      </div>
      <div className="__middle">
        <div className={'d-flex align-items-center work-space-sub'}>
          <i className={'fab fa-trello'}></i>
          <span className={'text-dark'}>Boards</span>
        </div>
        <div className={'d-flex align-items-center justify-content-between work-space-sub'}>
          {/*<Link to={'/w/' + props.workSpace.id + '/members'} className={'nav-link-custom'}>*/}
          <div>
            <i className="fas fa-user-friends"></i>
            <span className={'text-dark'}>Members</span>
          </div>
          {/*</Link>*/}
          <i className="fas fa-plus" onClick={() => props.openModalMember()}></i>
        </div>
        <div className={'d-flex align-items-center work-space-sub'}>
          {/*<Link to={'/w/' + props.workSpace.id + '/settings'} className={'nav-link-custom'}>*/}
            <i className="fal fa-cog"></i>
            <span className={'text-dark'}>Settings</span>
          {/*</Link>*/}
        </div>
      </div>
      <div className="__bottom">

      </div>
    </div>
  )
}

export default SideBar