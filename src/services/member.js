import http from './http'
import globalState from '../state/globalState'

export const get = () => {
  let data = [];
  return http.get('/workspace.json').then(response => {
    Object.entries(response.data).map(e => {
      data = data.concat({
        id: e[0],
        name: e[1].name,
        type: e[1].type,
        description: e[1].description,
      })
    })
    globalState.set(state => {
      return {
        ...state,
        workSpaces: data,
        loading: false
      }
    })
  })
}

export const post = (payload) => {
  return http.post('/workspace/' + payload.id + '/members.json', {email: payload.email}).then(response => {
    globalState.set(state => {
      return {
        ...state,
        loading: false
      }
    })
  })
}

export const destroy = (id) => {
  return http.delete('/workspace/' + id + '.json')
}

const memberService = {
  get: get,
  post: post,
  delete: destroy
}

export default memberService