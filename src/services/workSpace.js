import http from './http'
import globalState from '../state/globalState'

export const get = () => {
  let data = [];
  return http.get('/workspace.json').then(response => {
    Object.entries(response.data).map(e => {
      let members = [];
      if(e[1].members) {
        Object.entries(e[1].members).map(el => {
          members = members.concat({
            id: el[0],
            email: el[1].email
          })
        })
      }
      data = data.concat({
        id: e[0],
        name: e[1].name,
        type: e[1].type,
        description: e[1].description,
        members: members,
      })
    })
    globalState.set(state => {
      return {
        ...state,
        workSpaces: data,
        loading: false
      }
    })
  })
}

export const post = (payload) => {
  return http.post('/workspace.json', payload).then(response => {
    globalState.set(state => {
      return {
        ...state,
        loading: false
      }
    })
  })
}

export const destroy = (id) => {
  return http.delete('/workspace/' + id + '.json')
}

const workSpaceService = {
  get: get,
  post: post,
  delete: destroy
}

export default workSpaceService