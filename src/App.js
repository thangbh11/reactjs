import React from 'react';
import './App.css';
import Home from './pages/Home/Home';
import Menu from "./components/Menu";
import ReactLoading from 'react-loading';
import globalState from './state/globalState'
import {useHookstate} from "@hookstate/core";
import {Route, Routes} from "react-router-dom";
import Members from "./pages/WorkSpace/Members";
import Settings from "./pages/WorkSpace/Settings";

function App() {
  const state = useHookstate(globalState);
  const loading = state.loading.get()
  return (
    <>
      <div className={'loading ' + (loading ? 'd-flex' : 'd-none')}>
        <ReactLoading type={'bubbles'} color={'#026AA7'} height={'20px'} width={'40px'} />
      </div>
      <Menu />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="w/:id/members" element={<Members />} />
        <Route path="w/:id/settings" element={<Settings />} />
        {/*<Route path="admins/:id" element={<AdminChat />} />*/}
        {/*<Route path="admins" element={<AdminChat />} />*/}
        {/*<Route path="login" element={<AdminForm />} />*/}
      </Routes>
    </>
  );
}

export default App;
