export const workSpaceTypes = [
  {
    value: '',
    name: 'Choose...'
  },
  {
    value: 1,
    name: 'Marketing'
  },
  {
    value: 2,
    name: 'Human Resources'
  },
  {
    value: 3,
    name: 'Operations'
  },
  {
    value: 4,
    name: 'Engineering-IT'
  },
  {
    value: 5,
    name: 'Small Business'
  },
  {
    value: 6,
    name: 'Sales CRM'
  },
  {
    value: 7,
    name: 'Education'
  },
  {
    value: 8,
    name: 'Other'
  }
];

