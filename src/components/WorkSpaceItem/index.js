import './WorkSpaceItem.scss'
import {useEffect, useState} from "react";
import globalState from "../../state/globalState";
import {Link} from "react-router-dom";

function WorkSpaceItem (props) {
  const [open, setOpen] = useState(props.open)
  return (
    <>
      <div className={'d-flex __header __title'} onClick={() => setOpen(!open)}>
        <div className={'d-flex'}>
          <div className={'rounded-0 icon-name text-center bg-' + props.index}>
            <b>{props.workSpace.name.charAt(0)}</b>
          </div>
          <b className={'text-dark'}>{props.workSpace.name}</b>
        </div>
        <button className={'rounded border-0 bg-transparent'}>
          <i className={'fas ' + (open ? 'fa-chevron-up' : 'fa-chevron-down')}></i>
        </button>
      </div>
      <div className={'__content ' + (open ? '' : 'd-none')}>
        <div className={'d-flex align-items-center work-space-sub'}>
          <i className={'fab fa-trello'}></i>
          <span className={'text-dark'}>Boards</span>
        </div>
        <div className={'d-flex align-items-center work-space-sub'}>
          <i className="far fa-heart"></i>
          <span className={'text-dark'}>Highlights</span>
        </div>
        <div className={'d-flex align-items-center work-space-sub'}>
          <i className="far fa-table"></i>
          <span className={'text-dark'}>Views</span>
        </div>
        <div className={'d-flex align-items-center justify-content-between work-space-sub'}>
          <Link to={'/w/' + props.workSpace.id + '/members'} className={'nav-link-custom'}>
            <i className="fas fa-user-friends"></i>
            <span className={'text-dark'}>Members</span>
          </Link>
          <i className="fas fa-plus" onClick={() => props.openModalMember()}></i>
        </div>
        <div className={'d-flex align-items-center work-space-sub'}>
          <Link to={'/w/' + props.workSpace.id + '/settings'} className={'nav-link-custom'}>
          <i className="fal fa-cog"></i>
          <span className={'text-dark'}>Settings</span>
          </Link>
        </div>
      </div>
    </>
  )
}

export default WorkSpaceItem
