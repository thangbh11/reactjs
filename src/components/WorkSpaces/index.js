import './WorkSpaces.scss'
import {useEffect, useState} from "react";
import WorkSpaceItem from "../WorkSpaceItem";
import ModalAddWorkSpace from "../ModalAddWorkSpace";
import workSpaceService from "../../services/workSpace";
import globalState from "../../state/globalState"
import ModalAddMember from "../Members/ModalAddMember";

function WorkSpaces () {
  const [show, setShow] = useState(false)
  const [workSpaceId, setWorkSpaceId] = useState(false)
  const [showModalMember, setShowModalMember] = useState(false)
  const workSpaces = globalState.workSpaces.get()
  useEffect(() => {
    globalState.loading.set(true)

    workSpaceService.get()
  }, [show]);

  const hideModalMember = () => {
    setShowModalMember(false)
  }

  const openModalMember = async (id) => {
    await setWorkSpaceId(id)
    setShowModalMember(true)
  }

  return (
    <>
      <ModalAddWorkSpace show={show} setShow={setShow} />
      <ModalAddMember show={showModalMember} hideModalMember={hideModalMember} workSpaceId={workSpaceId} />
      <div className={'work-space'}>
        <div className={'d-flex __header'}>
          <span>Workspaces</span>
          <button className={'rounded border-0'} onClick={() => setShow(true)}>
            <i className="fas fa-plus"></i>
          </button>
        </div>
        {
          workSpaces && workSpaces.map((item, key) => (
            <WorkSpaceItem index={key} workSpace={item} key={key} open={!key} openModalMember={() => openModalMember(item.id)}/>
          ))
        }
      </div>
    </>
  )
}

export default WorkSpaces
