import {useState} from "react";
import memberService from "../../services/member"
import globalState from "../../state/globalState"

const ModalAddMember = (props) => {
  const [email, setEmail] = useState('')
  const [valid, setValid] = useState(false)
  const workSpaceId = props.workSpaceId
  const showModal = globalState.modalMembers?.showModal?.get()
  const handleClick = async () => {
    if (valid) {
      props.hideModalMember()
      globalState.loading.set(true)
      await memberService.post({email: email, id: workSpaceId})
      setEmail('')
    }
  }

  const validateEmail = (email) => {
    if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
      setValid(true)
    } else {
      setValid(false)
    }
  }

  return (
    <>
      <div className={'modal-backdrop fade show ' + (props.show ? 'd-block' : 'd-none')}></div>
      <div className={'modal ' + (props.show ? 'd-block' : '')} tabIndex="-1">
        <div className="modal-dialog">
          <div className="modal-content py-4 px-3">
            <span className={'__title'}>Invite to Workspace</span>
            <input type="text" className="__input" maxLength="100" placeholder="Email address" value={email} onChange={e => {
              setEmail(e.target.value)
              validateEmail(e.target.value)
            }}/>
            <button onClick={handleClick} className={'w-100 __button my-2 ' + (valid ? 'bg-primary text-white' : 'not-allowed' )}>Send invite</button>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={() => props.hideModalMember()}></button>
          </div>
        </div>
      </div>
    </>
  )
}

export default ModalAddMember