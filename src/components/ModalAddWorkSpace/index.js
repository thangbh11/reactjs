import './ModalAddWorkSpace.scss';
import {workSpaceTypes} from '../../config/workSpaceTypes'
import {useState} from "react";
import workSpace from "../../services/workSpace";

const ModalAddWorkSpace = (props) => {
  const [name, setName] = useState('');
  const [type, setType] = useState('');
  const [description, setDescription] = useState('');
  const handleClick = () => {
    workSpace.post({name, type, description, members: [{email: 'thangbh1@vmodev.com'}]})
    props.setShow(false)
    setName('')
    setType('')
    setDescription('')
  }
  return (
    <>
      <div className={'modal-backdrop fade show ' + (props.show ? 'd-block' : 'd-none')}></div>
      <div className={'modal ' + (props.show ? 'd-block' : '')} tabIndex="-1">
        <div className="modal-dialog modal-xl">
          <div className="modal-content">
            <div className="row">
              <div className="col __left">
                <div className="w-75">
                  <span className={'__title'}>Let's build a Workspace</span>
                  <span className={'__description'}>Boost your productivity by making it easier for everyone to access boards in one location.</span>

                  <label className="__label">Workspace name</label>
                  <input type="text" className="__input" maxLength="100" placeholder="Taco's Co." value={name} onChange={e => setName(e.target.value)}/>
                  <span className={'__note'}>This is the name of your company, team or organization.</span>

                  <label className="__label">Workspace type</label>
                  <select className={'__select'} value={type} onChange={e => setType(e.target.value)}>
                    {
                      workSpaceTypes && workSpaceTypes.map(elm => <option
                      key={elm.value}
                      value={elm.value}>{elm.name}</option>)
                    }
                  </select>

                  <label className="__label">Workspace description</label>
                  <textarea placeholder={'Our team organizes everything here.'}
                    onChange={e => setDescription(e.target.value)}
                    className={'__textarea'}
                    id=""
                    rows="6"
                    defaultValue={description}></textarea>
                  <span className={'__note'}>Get your members on board with a few words about your Workspace.</span>
                  <button onClick={handleClick} className={'w-100 __button my-2 ' + (name && type ? 'bg-primary text-white' : 'not-allowed' )}>Continue</button>
                </div>
              </div>
              <div className="col __right">
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={() => props.setShow(false)}></button>
                <img width="342" height="256" src="https://a.trellocdn.com/prgb/dist/images/organization/empty-board.d1f066971350650d3346.svg" alt="" role="presentation" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default ModalAddWorkSpace
