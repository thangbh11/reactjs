import NavBarItem from "../NavBarItem";
import './SideBar.scss'
import WorkSpaces from "../WorkSpaces";

function SideBar () {
  return (
    <div className={'nav-bar'}>
      <NavBarItem icon={'fab fa-trello'} title={'Boards'} active={true} />
      <NavBarItem icon={'fas fa-table'} title={'Templates'} />
      <NavBarItem icon={'fas fa-heart-rate'} title={'Home'} />
      <WorkSpaces />
    </div>
  )
}

export default SideBar