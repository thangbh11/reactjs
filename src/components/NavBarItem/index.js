import './NavBarItem.scss';
import clsx from 'clsx'

function NavBarItem (props) {
  return (
    <div className={clsx({
      'navbar-item': true,
      'active': props.active
    })}>
      <i className={props.icon}></i>
      <b>{props.title}</b>
    </div>
  )
}

export default NavBarItem