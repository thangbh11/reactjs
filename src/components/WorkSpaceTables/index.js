import './WorkSpaceTables.scss'
import workSpaceService from "../../services/workSpace";
import globalState from "../../state/globalState";
import {Link} from "react-router-dom";

const WorkSpaceTables = (props) => {
  const handleClick = async (id) => {
    globalState.loading.set(true)
    await workSpaceService.delete(id)
    await workSpaceService.get()
  }

  return (
    <div className={'mb-4'}>
      <div className={'d-flex justify-content-between work-space-tables'}>
        <div className="d-flex">
          <div className={'rounded-0 icon-name text-center bg-' + props.index}>
            <b>{props.workSpace.name.charAt(0)}</b>
          </div>
          <b className={'text-dark __name'}>{props.workSpace.name}</b>
        </div>
        <div className={'d-flex'}>
          <div className={'d-flex align-items-center work-space-sub'}>
            <i className={'fab fa-trello'}></i>
            <span className={'text-dark'}>Boards</span>
          </div>
          <div className={'d-flex align-items-center work-space-sub'}>
            <i className="far fa-table"></i>
            <span className={'text-dark'}>Views</span>
          </div>
          <div className={'d-flex align-items-center justify-content-between work-space-sub'}>
            <Link to={'/w/' + props.workSpace.id + '/members'} className={'nav-link-custom'}>
              <i className="fas fa-user-friends"></i>
              <span className={'text-dark'}>Members ({props.workSpace.members?.length})</span>
            </Link>
          </div>
          <div className={'d-flex align-items-center work-space-sub'}>
            <Link to={'/w/' + props.workSpace.id + '/settings'} className={'nav-link-custom'}>
              <i className="fal fa-cog"></i>
              <span className={'text-dark'}>Settings</span>
            </Link>
          </div>
          <div className={'d-flex align-items-center work-space-sub bg-danger text-white'} onClick={() => handleClick(props.workSpace.id)}>
            <i className="fal fa-trash"></i>
            <span className={'text-white'}>Delete</span>
          </div>
        </div>
      </div>
      <div className={'add-tables mt-2'}>
        <span>Create new board</span>
      </div>
    </div>
  )
}

export default WorkSpaceTables
